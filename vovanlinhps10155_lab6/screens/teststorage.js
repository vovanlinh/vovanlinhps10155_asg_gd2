import React from 'react';
import { Image, StyleSheet, Button, Text, View, Alert, } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as firebase from 'firebase';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
           
            image: '',
        }
    }
  static navigationOptions = {
    header: null,
  };

  onChooseImagePress = async () => {
   // let result = await ImagePicker.launchCameraAsync();
    //let result = await ImagePicker.launchImageLibraryAsync();
    let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
         allowsEditing: true,
         aspect: [4, 3],
         quality: 1
       });
    if (!result.cancelled) {
        this.setState({ image: result.uri });
      this.uploadImage(result.uri, "test-imageasas")
      .then((user) => {
                
        Alert.alert('Finish! ', 'Thêm thành công!', [], { cancelable: true });

    })
    .catch((error) => {
        const { code, message } = error;
        // For details of error codes, see the docs
        // The message contains the default Firebase string
        // representation of the error

        Alert.alert('Lỗi! ', 'Vui lòng thử lại', [], { cancelable: true })

    });
    }
  }

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();

    var ref = firebase.storage().ref().child("images/" + imageName);
    return ref.put(blob);
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="Choose image..." onPress={this.onChooseImagePress} />
        <Image source={{ uri: this.state.image }} style={styles.pickimage} />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, paddingTop: 50, alignItems: "center", },
  pickimage: { width:100,height:100, },
});