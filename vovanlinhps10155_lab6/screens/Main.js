import React from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TextInput,
    Button,
    FlatList,
    Alert,
    ListView,
    TouchableOpacity,
    Image
} from 'react-native'
import Dialog from "react-native-dialog";
import firebase from '../firebase/firebase.js';
import Modal from 'react-native-modal'
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
export default class MainScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            postContent: '',
            post: [],
            modalVisible: false,
            modalVisibleUpdate: false,
            dialogVisible: false,
         

        }
        this.itemsRef = firebase.database().ref().child('books');
    }
    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
          const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      }
    _showDialog = () => {
        this.setState({ modalVisible: true })
    }
    _hideDialog = () => {
        this.setState({ modalVisible: false })
    }
    _showDialogUpdate = () => {
        this.setState({ modalVisibleUpdate: true })
    }
    _hideDialogUpdate = () => {
        this.setState({ modalVisibleUpdate: false })
    }
    _post = () => {
        this.setState({ modalVisible: true })
    }
    _showDialogdelete = () => {
        this.setState({ dialogVisible: true });
    };

    _handleCanceldelete = () => {
        this.setState({ dialogVisible: false });
    };

    listenForItems(itemsRef) {
        itemsRef.on('value', (snap) => {
            var items = [];
            snap.forEach((child) => {
                let item = {
                    key: (child.key),
                    ten: child.val().ten,
                    gia: child.val().gia,
                    tacgia: child.val().tacgia,
                    soluong: child.val().soluong,
                    nxb: child.val().nxb,
                }
                items.push(item);
            });


            this.setState({
                post: items
            });
            this.state.post.map((item, idx) => {
                console.log(item.key)
            })

        });
    }

    _delete(key) {
        firebase.database().ref().child('books').child(key).remove()
            .then((user) => {
                this.setState({ dialogVisible: false });
                Alert.alert('Finish! ', 'Xóa thành công!', [], { cancelable: true });

            })
            .catch((error) => {
                const { code, message } = error;
                // For details of error codes, see the docs
                // The message contains the default Firebase string
                // representation of the error

                Alert.alert('Lỗi! ', 'Vui lòng thử lại', [], { cancelable: true })

            });
    }

    _edit(key) {
        firebase.database().ref().child('books').child(key).set({
            ten: 'post',
            gia: '200',
            tacgia: 'da update',
            soluong: 'da update',
            nxb: 'da update',
        })

    }

    componentWillMount() {
        this.getPermissionAsync();
        this.listenForItems(this.itemsRef)
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    isVisible={this.state.modalVisible}
                    onBackButtonPress={this._hideDialog}
                    onBackdropPress={this._hideDialog}>

                    <InsertProduct />
                </Modal>
                <View style={styles.btninsert}>
                    <TouchableOpacity
                        onPress={() => {
                            this._showDialog()
                        }}  >
                        <Text style={styles.textinsert}>Insert</Text>
                    </TouchableOpacity>


                </View>
                <FlatList
                    data={this.state.post}

                    renderItem={({ item }) =>
                        <View style={styles.postContainer}>
                            <Text> {item.ten} - {item.gia} - {item.tacgia}- {item.nxb}- {item.soluong} </Text>
                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 3
                                }}
                                onPress={this._showDialogdelete}>
                                <Text>Delete</Text>
                            </TouchableOpacity>
                            <Dialog.Container visible={this.state.dialogVisible}>
                                <Dialog.Title>Delete</Dialog.Title>
                                <Dialog.Description>
                                    Bạn có muốn xóa?
          </Dialog.Description>
                                <Dialog.Button label="Cancel" onPress={this._handleCanceldelete} />
                                <Dialog.Button label="Delete" onPress={() => {
                                    this._delete(item.key)
                                }}
                                />
                            </Dialog.Container>





                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 50
                                }}
                                onPress={() => {
                                    this._showDialogUpdate()
                                }}>

                                <Text>Update</Text>
                            </TouchableOpacity>
                            <Modal
                    animationType="slide"
                    transparent={false}
                    isVisible={this.state.modalVisibleUpdate}
                    onBackButtonPress={this._hideDialogUpdate}
                    onBackdropPress={this._hideDialogUpdate}>

                    <UpdateProduct />
                </Modal>

                        </View>


                    }
                />

            </View>
        )

    }
}


const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        width: null,
        height: null,
    },
    postStatus: {
        backgroundColor: '#432577',
        marginTop: 30,
    },
    postContainer: {
        backgroundColor: '#f1f1f1',
        margin: width * 3.6 / 187.5,
        padding: width * 3.6 / 187.5,
        borderRadius: width * 3.6 / 187.5,
    },
    nameAuth: {
        color: '#000',
        fontSize: 16
    },
    postText: {
        color: '#000',
        // backgroundColor:'#fff',
        padding: width * 3.6 / 187.5
    },
    insertContainer: {
        width: width * 167.5 / 187.5,

        backgroundColor: '#fff',
        padding: width * 5 / 187.5
    },
    insertRow: {
        flexDirection: 'row', alignItems: 'center',
        marginBottom: 20,
    },
    insertTextinput: {
        fontSize: 16,
        borderColor: 'white',
        borderWidth: 0.5,
        flex: 1,

    },
    btninsert: {
        marginTop: 30,
        height: 45,
        borderRadius: 15,
        backgroundColor: '#432577',
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 0.5,
    },
    textinsert: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',

    },
    pickimage: {
        borderColor: 'black',
        borderWidth: 0.5,
        marginLeft:40,
        justifyContent: 'center',
        backgroundColor: '#f1f1f1',
        width:150,
        height:120,
    },

})





export class InsertProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ten: '',
            tacgia: '',
            soluong: '',
            gia: '',
            nxb: '',
            image: null,
            imageurl:'',
           
        }
    }

    onChooseImagePress = async () => {
        // let result = await ImagePicker.launchCameraAsync();
         //let result = await ImagePicker.launchImageLibraryAsync();
         let result = await ImagePicker.launchImageLibraryAsync({
             mediaTypes: ImagePicker.MediaTypeOptions.All,
              allowsEditing: true,
              aspect: [4, 3],
              quality: 1
            });
         if (!result.cancelled) {
             this.setState({ image: result.uri });
           /*
             this.uploadImage(result.uri, "test-image5")
           .then((user) => {
                     
             Alert.alert('Finish! ', 'Thêm thành công!', [], { cancelable: true });
     
         })
         .catch((error) => {
             const { code, message } = error;
             // For details of error codes, see the docs
             // The message contains the default Firebase string
             // representation of the error
     
             Alert.alert('Lỗi! ', 'Vui lòng thử lại', [], { cancelable: true })
     
         });
         */
         }
       }
     
       uploadImage = async (uri, imageName) => {
         const response = await fetch(uri);
         const blob = await response.blob();
     
         var ref = firebase.storage().ref().child("images/" + imageName);

         return ref.put(blob);
       }
     

    _post = () => {
        
        console.log('post', new Date().getTime())
const linkanh=this.state.image;
this.uploadImage(linkanh, this.state.ten);
        firebase.database().ref().child('books').push({

            ten: this.state.ten,
            tacgia: this.state.tacgia,
            soluong: this.state.soluong,
            gia: this.state.gia,
            nxb: this.state.nxb,
            imageurl: this.state.imageurl,
        })
            .then((user) => {
                this.ten.clear();
                this.gia.clear();
                this.tacgia.clear();
                this.soluong.clear();
                this.nxb.clear();
                
                Alert.alert('Finish! ', 'Thêm thành công!', [], { cancelable: true });

            })
            .catch((error) => {
                const { code, message } = error;
                // For details of error codes, see the docs
                // The message contains the default Firebase string
                // representation of the error

                Alert.alert('Lỗi! ', 'Vui lòng thử lại', [], { cancelable: true })

            });
          
    }

    
    render() {
        //let { image } = this.state;
        return (
            <View style={styles.insertContainer}>
                <View style={styles.insertRow}>
                    <Text>Tên sản phẩm:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ ten: value }) }}
                        ref={input => { this.ten = input }}

                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Giá:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ gia: value }) }}
                        ref={input => { this.gia = input }}
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Tác giả:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ tacgia: value }) }}
                        ref={input => { this.tacgia = input }}
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Nhà xuất bản:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ nxb: value }) }}
                        ref={input => { this.nxb = input }}
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Số lượng:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ soluong: value }) }}
                        ref={input => { this.soluong = input }}
                    />
                </View>
                <View style={styles.insertRow}>
               
                <Button
          title="Pick image"
          onPress={this.onChooseImagePress}
        />
        
          <Image source={{ uri: this.state.image }} style={styles.pickimage}  />

                </View>


                <View style={styles.btninsert}>
                    <TouchableOpacity
                        onPress={() => { this._post() }}>
                        <Text style={styles.textinsert}>Save</Text>
                    </TouchableOpacity>


                </View>
            </View>
        )
    }
}

export class UpdateProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ten:'',
            tacgia: '',
            soluong: '',
            gia: '',
            nxb: ''
        }
    }
    _edit(key) {


        firebase.database().ref().child('books').child(key).set({

            ten: this.state.ten,
            tacgia: this.state.tacgia,
            soluong: this.state.soluong,
            gia: this.state.gia,
            nxb: this.state.nxb
        })
    }

    render() {
        return (
            
            <View style={styles.insertContainer}>
               <View style={styles.insertRow}>
                    <Text>Tên sản phẩm:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ ten: value }) }}
                        ref={input => { this.ten = input }}

                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Giá:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ gia: value }) }}
                       
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Tác giả:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ tacgia: value }) }}
                       
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Nhà xuất bản:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ nxb: value }) }}
                       
                    />
                </View>
                <View style={styles.insertRow}>
                    <Text>Số lượng:</Text>
                    <TextInput
                        style={styles.insertTextinput}

                        onChangeText={(value) => { this.setState({ soluong: value }) }}
                        
                    />
                </View>


                <View style={{ flexDirection: 'row', margintop: width * 3.6 / 187.5, alignItems: 'center', justifyContent: 'center' }}>

                    <Button style={{ width: width * 80 / 187.5 }} title='Submit' onPress={() => { this._edit(item.key) }} />
                </View>
            </View>
        )
    }
}
